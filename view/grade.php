<div class="container grade-css">
    <h1>Clientes</h1>
    <hr>
    <table class="table table-bordered table-striped" style="top:40px;">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Telefone</th>
                <th>Endereço de origem</th>
                <th>Endereço de destino</th>
                <th><a href="?controller=ClientsController&method=create" class="btn btn-success btn-sm">Novo</a></th>
            </tr>
        </thead>
        <tbody>
            <?php
            if ($clients) {
                foreach ($clients as $client) {
                    ?>
                    <tr>
                        <td><?php echo $client->name; ?></td>
                        <td><?php echo $client->phone; ?></td>
                        <td><?php echo $client->source_address; ?></td>
                        <td><?php echo $client->destination_address; ?></td>
                        <td>
                            <a href="?controller=ClientsController&method=edit&id=<?php echo $client->id; ?>" class="btn btn-primary btn-sm">Editar</a>

                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#modalDelete">
                                Excluir
                            </button>

                            <!-- Modal -->
                            <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Deseja excluir?</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                            <a href="?controller=ClientsController&method=delete&id=<?php echo $client->id; ?>" class="btn btn-danger btn-sm">Confirmar</a>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="alert" role="alert" id="result"></div>

                            <div class="alert" role="alert" id="result"></div>
                        </td>
                    </tr>
                <?php
                    }
                } else {
                    ?>
                <tr>
                    <td colspan="5">Nenhum registro encontrado</td>
                </tr>
            <?php
            }
            ?>
        </tbody>
    </table>
</div>